/**
 * @author Bruker
 *
 */
public class HolidaySaleVisitor implements Visitor {

	@Override
	public float visit(Trousers trousers) {
		System.out.println("Trousers: Holiday sale price");
		return (float) (trousers.getPrice() * 0.30);
	}

	@Override
	public float visit(Hat hat) {
		System.out.println("Hat: Holiday sale price");
		return (float) (hat.getPrice() * 0.20);
	}

	@Override
	public float visit(Sock sock) {
		System.out.println("Sock: Holiday sale price");
		return (float) (sock.getPrice() * 0.20);
	}

}
