/**
 * 
 */

/**
 * @author Bruker
 *
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		NormalPriceVisitor normal = new NormalPriceVisitor();
		HolidaySaleVisitor holiday = new HolidaySaleVisitor();
		
		Trousers yellowTrouser = new Trousers(30);
		Hat greenHat = new Hat(20);
		Sock blackSocks = new Sock(15);
		
		System.out.println(yellowTrouser.getTotalPrice(normal));
		System.out.println(greenHat.getTotalPrice(normal));
		System.out.println(blackSocks.getTotalPrice(normal));
		
		System.out.println("\nHoliday sale:");
		
		System.out.println(yellowTrouser.getTotalPrice(holiday));
		System.out.println(greenHat.getTotalPrice(holiday));
		System.out.println(blackSocks.getTotalPrice(holiday));
	}

}
