/**
 * 
 */

/**
 * @author Bruker
 *
 */
public class Hat implements Visitable{
	private float price;
	
	public Hat(float price) {
		this.price = price;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}
	
	@Override
	public float getTotalPrice(Visitor visitor) {
		return visitor.visit(this);
	}
}
