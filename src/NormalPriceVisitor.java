/**
 * 
 */

/**
 * @author Bruker
 *
 */
public class NormalPriceVisitor implements Visitor {

	@Override
	public float visit(Trousers trousers) {
		System.out.println("Trousers: Normal price");
		return trousers.getPrice();
	}

	@Override
	public float visit(Hat hat) {
		System.out.println("Hat: Normal price");
		return hat.getPrice();
	}

	@Override
	public float visit(Sock sock) {
		System.out.println("Sock: Normal price");
		return sock.getPrice();
	}
}
