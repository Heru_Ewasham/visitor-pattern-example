/**
 * 
 */

/**
 * @author Bruker
 *
 */
public interface Visitable {
	public float getTotalPrice(Visitor visitor);
}
