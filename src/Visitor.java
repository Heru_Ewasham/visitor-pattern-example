/**
 * 
 */

/**
 * @author Bruker
 *
 */
public interface Visitor {
	public float visit(Trousers trousers);
	public float visit(Hat hat);
	public float visit(Sock sock);
}
