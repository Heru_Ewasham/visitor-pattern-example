/**
 * 
 */

/**
 * @author Bruker
 *
 */
public class Trousers implements Visitable{
	private float price;

	public Trousers(float price) {
		this.price = price;
	}
	
	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	@Override
	public float getTotalPrice(Visitor visitor) {
		return visitor.visit(this);
	}
}
